from flask import Flask
from flask import render_template
from flask import request
from datetime import datetime
from pymongo import MongoClient
import json


app = Flask(__name__)

mongo_host = 'localhost'
mongo_port = 27017
mongo_db = 'twitter'
mongo_col = 'output'

output = MongoClient(mongo_host, mongo_port)[mongo_db][mongo_col]

date = datetime.now()
time = date.hour
algorithm = "KMeans"
cluster = 0

@app.route("/", methods=['GET'])
def index():
	return render_template("index.html")

@app.route("/hour/", methods=['GET'])
def hour():
	return render_template("hour.html")

@app.route("/cluster/", methods=['GET'])
def cluster():
	return render_template("cluster.html")

if __name__ == "__main__":
	app.run(host='0.0.0.0', port=3000, debug=True)
