var w = screen.width;
var h = (screen.height*3)/4;
var padding = {
  top: 100,
  bottom: 20,
  left: 60,
  right: 60
};

var dataset = [];

d3.csv("static/scripts/data.csv", function(data) {
  dataset = data.map(function(d) {
    return [ Date.parse(d["time"]),
              +d["sentiment"],
              +d["user_statuses_count"]];
  });

  dataset = dataset.slice(396, dataset.length);

  var svg = d3.select("body")
              .append("svg")
              .attr("width", w)
              .attr("height", h);

  var xScale = d3.scaleLinear()
              .domain([d3.min(dataset, function(d){return d[0];}),
                       d3.max(dataset, function(d){return d[0];})])
              .range([padding.left, w - padding.right * 2]);

  var yScale = d3.scaleLinear()
              .domain([d3.min(dataset, function(d){return d[1];}),
                       d3.max(dataset, function(d){return d[1];})])
              .range([h - padding.bottom, padding.top]);

  var rScale = d3.scaleLinear()
                       .domain([0, d3.max(dataset, function(d) { return d[2]; })])
                       .range([2, 5]);

  var redScale = d3.scaleLinear()
                    .domain([d3.min(dataset, function(d){return d[2];}),
                             d3.max(dataset, function(d){return d[2];})])
                    .range([0,255]);

  var greenScale = d3.scaleLinear()
                  .domain([d3.min(dataset, function(d){ return d[2];}),
                           d3.max(dataset, function(d){ return d[2];})])
                  .range([255,0]);

  var blueScale = d3.scaleLinear()
                  .domain([d3.min(dataset, function(d){ return d[2];}),
                           d3.max(dataset, function(d){ return d[2];})])
                  .range([0,0]);

  var xAxis = d3.axisBottom()
                    .scale(xScale)
                    .ticks(5);

  svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + (h - padding.bottom) + ")")
        .attr("stroke", "orange")
        .call(xAxis);

  var yAxis = d3.axisLeft()
                    .scale(yScale)
                    .ticks(5);

  svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(" + padding.left + ",0)")
        .attr("stroke", "orange")
        .call(yAxis);

  svg.selectAll("circle")
        .data(dataset)
        .enter().append("circle")
              .attr("cx", function(d){return xScale(d[0]);})
              .attr("cy", function(d){return yScale(d[1]);})
              .attr("r", function(d){return rScale(d[2]);})
              .style("fill", function(d) {
                r = redScale(d[2]);
                g = greenScale(d[2]);
                b = blueScale(d[2]);
                return "rgb(" + r + ", " + g + ", " + b + ")"
              })
              .on("mouseover", function(d) {
                d3.select(this).style({opacity : '1.0'});
              });
});
