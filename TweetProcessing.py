from pymongo import MongoClient
import pandas as pd
from pandas.io.json import json_normalize
import numpy as np
from sklearn import cluster, model_selection
import scipy
from textblob import TextBlob, Word
import json
from datetime import datetime
import sys
import warnings
import re

client = MongoClient('localhost', 27017) # Update to actual database
db = client.twitter
input_collection = db.input
output_collection = db.output

start_time = str(datetime.now())[:19]

print("Loading Tweets %s" % datetime.now())

tweets_df = json_normalize(list(input_collection.find())).fillna(value="")

print("Tweets Loaded %s" % datetime.now())

input_collection.remove({})

print("Database cleaned %s" % datetime.now())

# Separate usable fields by how they will be cleaned
as_is = ["reply_count", "timestamp_ms", "user.favourites_count", "user.followers_count",
	"user.friends_count", "user.listed_count", "user.statuses_count"]

dates = ["created_at", "user.created_at"]

not_blank = ["geo.coordinates", "place.id", "quoted_status.created_at", "user.description",
	"user.location", "user.profile_background_image_url", "user.profile_banner_url", "user.url"]

is_true_text = ["possibly_sensitive", "truncated", "favorited", "retweeted", "is_quote_status",
	"user.contributors_enabled", "user.default_profile", "user.default_profile_image", "user.geo_enabled",
	"user.is_translator", "user.profile_background_tile", "user.profile_use_background_image",
	"user.protected", "user.time_zone", "user.verified"]

is_regular = ["user.translator_type"]

not_null = ["in_reply_to_user_id"]

is_en = ["lang", "user.lang"]

make_blanks_zero = ["quoted_status.favorite_count"]

# Add back dates?
usable_fields = as_is + dates + not_blank + is_true_text + is_regular + not_null + is_en + make_blanks_zero

predict = ["favorite_count", "retweet_count"]

for i in range(0,len(predict)):
	predict[i] = [predict[i], tweets_df[predict[i]]]

def update_all(func, fields):
	for field in fields:
		try:
			tweets_df[field] = list(map(func, tweets_df[field]))
		except:
			print("No Field: %s" % field)
			usable_fields.remove(field)

def to_date(date_str):
	try:
		d = date_str[4:]
		d = d[:16] + d[22:]
		d = datetime.strptime(d, '%b %d %H:%M:%S %Y')
		return(str(d)[:19])
	except:
		return(str(datetime.fromtimestamp(int(date_str)))[:19])

def is_true(x):
	if x:
		return 1
	else:
		return 0

def is_true_string(s):
	return(is_true((str(s) == "True") | (str(s) == "TRUE")))

def blank(s):
	return(is_true(s != ""))

def regular(s):
	return(is_true(str(s) == "regular"))

def null(s):
	return(is_true(s != None))

def en(s):
	return(is_true(s == "en"))

def blanks_to_zero(s):
	if(str(s) == ""):
		return(0)
	elif s == None:
		return(0)
	else:
		return(int(s))

update_all(to_date, dates)
update_all(blank, not_blank)
update_all(is_true_string, is_true_text)
update_all(regular, is_regular)
update_all(null, not_null)
update_all(en, is_en)
update_all(blanks_to_zero, make_blanks_zero)

print("Counting Entites %s" % datetime.now())

entity_file = open("entities.txt", "r")
for line in entity_file:
	line = line[0:len(line)-1]
	name, words = line.split(':')
	words = words.split(",")

	def count_words(text):
		count = 0
		for w in words:
			count = count + sum(1 for _ in re.finditer(w.lower(), text.lower()))

		return(count)

	tweets_df[name] = list(map(count_words, tweets_df['text']))
	usable_fields.append(name)

sentiment = list(map(lambda t: TextBlob(t).sentiment.polarity, tweets_df['text']))
tweets_df['sentiment'] = sentiment
usable_fields.append('sentiment')

created_at = tweets_df['created_at']
texts = tweets_df['text']

usable_fields.remove("created_at")
usable_fields.remove("user.created_at")

tweets_df = tweets_df[usable_fields]

# Create cluster objects
models = []
models.append(['KMeans', cluster.KMeans(n_clusters=4)])
# models.append(['AffinityPropagation', cluster.AffinityPropagation(damping=.9, preference=-200)])
models.append(['MeanShift', cluster.MeanShift()])
models.append(['Spectral', cluster.SpectralClustering(n_clusters=4)])
models.append(['Ward', cluster.AgglomerativeClustering(n_clusters=4, linkage='ward')])
models.append(['DBSCAN', cluster.DBSCAN()])
models.append(['Birch', cluster.Birch(n_clusters=4)])

# Train clusters
split = 0.8
tweets_df_train, tweets_df_test = model_selection.train_test_split(tweets_df, train_size=split, test_size=(1-split))

for name, model in models:
	print("Training %s" % name)
	with warnings.catch_warnings():
		warnings.filterwarnings(
			"ignore",
			message="Graph is not fully connected, spectral embedding" +
			" may not work as expected.",
			category=UserWarning)
		model.fit(tweets_df)

	if hasattr(model, 'labels_'):
		y_pred = model.labels_.astype(np.int)
	else:
		y_pred = model.predict(tweets_df_train)

	tweets_df[name] = y_pred

time_array = []
for i in range(0,len(tweets_df)):
	time_array.append(start_time)
tweets_df['time'] = time_array

tweets_df['created_at'] = created_at
tweets_df['text'] = texts

for field, values in predict:
	tweets_df[field] = values

col_names = list(tweets_df.columns.values)
new_col_names = list(map(lambda x: x.replace('.', '_'), col_names))
tweets_df.columns = new_col_names

print("Begining Upload %s" % datetime.now())

for i in range(0, len(tweets_df)):
	output_collection.insert(tweets_df.T[i].to_dict())

print("Output Complete %s" % datetime.now())