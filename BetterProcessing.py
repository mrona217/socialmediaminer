from pymongo import MongoClient
import pandas as pd
from pandas.io.json import json_normalize
from nltk.corpus import stopwords
from nltk.stem import PorterStemmer, SnowballStemmer
from nltk.tokenize import word_tokenize
import textmining
import re
from scipy.spatial.distance import euclidean
from sklearn import cluster

# Connect to the database
client = MongoClient('localhost', 27017) # Update to actual database
db = client.twitter
input_collection = db.input
# output_collection = db.output

print("Connected to Database")

# Create a dataframe of flattened json objects from the imput collection
tweets_df = json_normalize(list(input_collection.find())).fillna(value="")

print("Tweets Loaded")

# Filter for only english tweets
tweets_df = tweets_df[tweets_df['lang'] == 'en']

# Create a term document matrix
tdm = textmining.TermDocumentMatrix()
for text in tweets_df['text']:
	tdm.add_doc(text)

# Update the names of the columns
tdm_df = pd.DataFrame(list(tdm.rows())[1:])
tdm_df.columns = list(tdm.rows())[0]

print("Term Document Matrix Created")

#Get list of stop words
stop_words = set(stopwords.words("english"))
term_file = open("search_terms.txt", "r")
for t in term_file.readlines():
	stop_words.add(t[0:len(t)-1])

#Remove all stop words
for w in stop_words:
	if w in tdm_df:
		tdm_df = tdm_df.drop(w, axis=1)

# Remove all words containing bad substrings
bad_contains = ["t.co", "http"]
for w in bad_contains:
	tdm_df = tdm_df[list(filter(lambda x: w not in x, tdm_df.columns.values))]

print("Bad Words Removed")

# TODO: Word Stemming
ps = PorterStemmer("english")
ss = SnowballStemmer("english")

# TODO: Bigrams/Trigrams

# TODO: Sparcity Reduction

# Calculate Euclidean Distance
tdmm = tdm_df.as_matrix()
dists = []
origin = [0] * len(tdmm[0])
for row in tdmm:
	dists.append(euclidean(origin, row))

print("Distance Calculated")

# Create dataframe with distance anc id_str
clean_data = []
for i in range(0, len(dists)):
	clean_data.append([list(tweets_df['id_str'])[i], dists[i]])

to_cluster = pd.DataFrame(clean_data, columns=['str_id', 'distance'])

# Cluster on distance
kmeans = cluster.KMeans(n_clusters=4)
kmeans.fit(to_cluster)

print("Clustered")

# Combine data sources
tweets_df['cluster'] = kmeans.labels_
tweets_df.index = range(0, len(tweets_df))
tdm_df.index = range(0, len(tdm_df))
tweets_df = pd.concat([tweets_df, tdm_df], axis=1)

print("Formatted")




















