#Import the necessary methods from tweepy library
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json
from pymongo import MongoClient
import sys
import datetime, time
import subprocess
import threading

#Variables that contains the user credentials to access Twitter API 
access_token = "523747593-xmbXpNbRPLxhkakK7sZHFVFnC2Xlur78hgyeiP3p"
access_token_secret = "1YRrI5xFqCVIGH2AC2bSd6OSSAq5t5dzwECVugGrR30UE"
consumer_key = "9pxgmS8EWFDpTVjAPMjfaXh9v"
consumer_secret = "tgJUDAHT4iaMAVcodOZXrlnzSd0cBDo7vDdWuQyc5LcZTtAQFb"

# Create a connection to the MongoDB database
# client = MongoClient('10.37.152.9', 27018)
client = MongoClient('localhost', 27017)
db = client.twitter
tweet_collection = db.input


# A listener to store tweets in MongoDB
class ComboListener(StreamListener):
	# Called when a tweet is recieved
	def on_data(self, data):
		# print to the console that a tweet has been recieved and a timestamp
		now = datetime.datetime.now()
		print("Tweet Received %s" % now)

		# extract the tweet from a json object to a dictionary
		tweet = json.loads(data)

		tweet['created_at'] = time.mktime(now.timetuple())

		# print(time.mktime(d.timetuple()))
		# filter out timestamps from the api
		if 'limit' in tweet:
			pass
		else:
			# Add the tweet to the database
			tweet_collection.insert(tweet)

		return True

	def on_error(self, status):
		print(status)

class tweetStreamThread(threading.Thread):

	# Create a new thread with the desired search terms
	def __init__(self, search_terms):
		threading.Thread.__init__(self)
		self.Search_Terms = search_terms

	def run(self):
		# Create a new listener
		l = ComboListener()

		# get api authentication
		auth = OAuthHandler(consumer_key, consumer_secret)
		auth.set_access_token(access_token, access_token_secret)

		# Stream tweets with search terms
		stream = Stream(auth, l)
		stream.filter(track=self.Search_Terms)

class rScriptThread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)

	def run(self):
		while True:
			# Every 10 minutes
			now = datetime.datetime.now()
			if((now.minute%10 == 0) & (now.second == 0)):
				print("Running python script")

				command = 'python3'
				path = 'TweetProcessing.py'
				args = []
				cmd = [command, path] + args

				# Call python Script command
				subprocess.call(cmd, universal_newlines=True)





if __name__ == '__main__':
	# Collect search terms from console input
	search_terms = []
	term_file = open("search_terms.txt", "r")
	terms = term_file.readlines()
	for t in terms:
		search_terms.append(t[0:len(t)-1])

	# Create the 2 threads
	twitter = tweetStreamThread(search_terms)
	rScript = rScriptThread()

	# Run the 2 threads
	twitter.start()
	rScript.start()